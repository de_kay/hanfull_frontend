import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Container from '@material-ui/core/Container'
import Toolbar from '@material-ui/core/Toolbar';
import HomeRoundedIcon from '@material-ui/icons/HomeRounded';
import IconButton from '@material-ui/core/IconButton';
// import Typography from '@material-ui/core/Typography';
// import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
// import MoreIcon from '@material-ui/icons/MoreVert';
import { Link } from 'react-router-dom';
import NavMenu from './NavMenu';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
    // minHeight: '50px',
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  desktopFont: {
    fontSize: '100%',
    color: 'inherit',
  },
  buttonTextLarge: {
    fontSize: '1.2rem',
  },
  toolbarScope: {
    minHeight: '45px',
    paddingRight: '10px',
    paddingLeft: '10px',
  }
}));

export default function NavBar() {
  const classes = useStyles();
  // const tokenContext = useContext(TokenContext);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };
  
  const handleMenuClose = () => {
    setAnchorEl(null);
  };
  
  return (
    <div className={classes.grow}>
      {/* 백그라운드 컬러 적용 AppBar <AppBar position="static" style={{backgroundColor: '#ffffff'}} > */}
      <AppBar position="static" color='inherit'  style={{backgroundColor: '#3f51b5'}}>
        <Toolbar className={classes.toolbarScope}>
          <Container className={classes.toolbarScope} maxWidth="false">
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              <Grid container spacing={0} alignItems="center" justify="left">
                <Grid item xs={1}>
                  <Link to="/">
                    <Button className={classes.buttonTextLarge} color="inherit">
                      <HomeRoundedIcon style={{fontSize: '2rem', color: 'white'}} />
                    </Button>
                  </Link>
                </Grid>
                <Grid item xs={1}>
                  <Link to="/order" underline='none' className={classes.desktopFont}>
                    <Button className={classes.buttonTextLarge} color="inherit" style={{color: '#ffffff'}}>
                      주문관리
                    </Button>
                  </Link>
                </Grid>
                <Grid item xs={1}>
                  <Link to="/supply" underline='none' className={classes.desktopFont}>
                    <Button className={classes.buttonTextLarge} color="inherit" style={{color: '#ffffff'}}>
                      발주관리
                    </Button>
                  </Link>
                </Grid>
                <Grid item xs={1}>
                  <Link to="/question" underline='none' className={classes.desktopFont}>
                    <Button className={classes.buttonTextLarge} color="inherit" style={{color: '#ffffff'}}>
                      문의관리
                    </Button>
                  </Link>
                </Grid>
                <Grid item xs={1}>
                  <Link to="/stats" underline='none' className={classes.desktopFont}>
                    <Button className={classes.buttonTextLarge} color="inherit" style={{color: '#ffffff'}}>
                      통계
                    </Button>
                  </Link>
                </Grid>
              </Grid>
            </div>
          </Container>
        </Toolbar>
      </AppBar>
    </div>
  );
}