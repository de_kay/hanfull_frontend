import React from 'react';
import {Link} from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuIcon from '@material-ui/icons/Menu';
import { Typography } from '@material-ui/core';

const options = [
    { name: 'HOME', url: '/' },
    { name: '주문관리', url: '/order' },
    { name: '발주관리', url: '/domestic-shop' },
    { name: '문의관리', url: '/overseas-shop' },
    { name: '통계', url: '/tour' },
];

const ITEM_HEIGHT = 100;

const useStyles = makeStyles(theme => ({
    menuButton: {
        marginRight: theme.spacing(0)
    },
}));

export default function NavMenu() {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const handleClick = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div>
            <IconButton
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="open drawer"
                onClick={handleClick}
            >
                <MenuIcon />
            </IconButton>
            <Menu
                id="long-menu"
                anchorEl={anchorEl}
                keepMounted
                open={open}
                onClose={handleClose}
                PaperProps={{
                    style: {
                        maxHeight: ITEM_HEIGHT * 4.5,
                        width: 200,
                    },
                }}
            >
                {options.map(option => (
                    <Link to={option.url} key={option.name}>
                        <MenuItem key={option.name} selected={option.url === '/'} onClick={handleClose} style={{backgroundColor:'white'}}>
                            <Typography style={{color:'black'}}>
                                {option.name}
                            </Typography>
                        </MenuItem>
                    </Link>
                ))}
            </Menu>
        </div>
    );
}