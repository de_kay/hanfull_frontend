import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import marketOrderData from './MarketOrderData';

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  { id: 'regDt', numeric: false, disablePadding: true, align: 'left', label: '주문일자' },
  { id: 'state', numeric: false, disablePadding: false, align: 'left', label: '상태' },
  { id: 'marketType', numeric: false, disablePadding: false, align: 'left', label: '마켓타입' },
  { id: 'channelNm', numeric: false, disablePadding: false, align: 'left', label: '채널명' },
  { id: 'orderer', numeric: false, disablePadding: false, align: 'left', label: '주문자' },
  { id: 'receiver', numeric: false, disablePadding: false, align: 'left', label: '수령자' },
  { id: 'goodsNm', numeric: false, disablePadding: false, align: 'left', label: '상품명' },
  { id: 'optionContent', numeric: false, disablePadding: false, align: 'left', label: '옵션' },
  { id: 'cnt', numeric: false, disablePadding: false, align: 'left', label: '수량' },
  { id: 'goodsPrice', numeric: true, disablePadding: false, align: 'right', label: '판매가' },
  { id: 'deliPrice', numeric: true, disablePadding: false, align: 'right', label: '배송비' },
  { id: 'deliType', numeric: false, disablePadding: false, align: 'left', label: '배송방법' },
  { id: 'deliCompanyNm', numeric: false, disablePadding: false, align: 'left', label: '배송사' },
  { id: 'invoice', numeric: false, disablePadding: false, align: 'left', label: '송장번호' },
];

function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'select all desserts' }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.align}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

export default function EnhancedTable({orderState, orderMarket, handleRowClick}) {
    const classes = useStyles();
    const [order, setOrder] = useState('asc');
    const [orderBy, setOrderBy] = useState('calories');
    const [selected, setSelected] = useState([]);
    const [page, setPage] = useState(0);
    const [dense, setDense] = useState(false);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [orderList, setOrderList] = useState([]);
    const [orderObj, setOrderObj] = useState({});

    useEffect(() => {
      if(orderMarket === 0 && orderState === 'ALL') {
        setOrderList(marketOrderData);
      } else if(orderMarket === 0 && orderState !== 'ALL') {
        const filterList = marketOrderData.filter(function(item) {
          return item.state.search(orderState) !== -1;
        });
        setOrderList(filterList);
      } else if(orderMarket !== 0 && orderState === 'ALL') {
        const filterList = marketOrderData.filter(function(item) {
          return item.nodeId.search(orderMarket) !== -1;
        });
        setOrderList(filterList);
      } else {
        const filterListOne = marketOrderData.filter(function(item) {
          return item.nodeId.search(orderMarket) !== -1;
        });
        const filterListTwo = filterListOne.filter(function(item) {
          return item.state.search(orderState) !== -1;
        });
        setOrderList(filterListTwo);
      }

    }, [orderState, orderMarket]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = orderList.map((n) => n.orderId);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };

  const isSelected = (name) => selected.indexOf(name) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, orderList.length - page * rowsPerPage);

  return (
    <div className={classes.root}>
        <TableContainer>
          {/* <FormControlLabel
            style={{textAlign: 'right'}}
            control={<Switch checked={dense} onChange={handleChangeDense} />}
            label="크게보기"
          /> */}
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            // size={dense ? 'small' : 'medium'}
            size={'small'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={orderList.length}
            />
            <TableBody>
              {stableSort(orderList, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.orderId);
                  const labelId = 'enhanced-table-checkbox-${index}';

                  return (
                    <TableRow
                    hover
                    onClick={() => handleRowClick(row)}
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={row.orderId}
                      selected={isItemSelected}
                      style={{cursor: 'pointer'}}
                      >
                      {/* <NodeContext.Consumer>
                      {value => nodeID}
                      </NodeContext.Consumer> */}
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          onClick={(event) => handleClick(event, row.orderId)}
                          inputProps={{ 'aria-labelledby': labelId }}
                        />
                      </TableCell>
                      <TableCell component="th" id={labelId} scope="row" padding="none">
                        {row.regDt}
                      </TableCell>
                      <TableCell align="left">{row.state}</TableCell>
                      <TableCell align="left">{row.marketType}</TableCell>
                      <TableCell align="left">{row.channelNm}</TableCell>
                      <TableCell align="left">{row.orderer}</TableCell>
                      <TableCell align="left">{row.receiver}</TableCell>
                      <TableCell align="left">{row.goodsNm}</TableCell>
                      <TableCell align="left">{row.optionContent}</TableCell>
                      <TableCell align="left">{row.cnt}</TableCell>
                      <TableCell align="right">{row.goodsPrice}</TableCell>
                      <TableCell align="right">{row.deliPrice}</TableCell>
                      <TableCell align="left">{row.deliType}</TableCell>
                      <TableCell align="left">{row.deliCompanyNm}</TableCell>
                      <TableCell align="left">{row.invoice}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={15} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={orderList.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
    </div>
  );
}