import React, { useState } from 'react';
import 'date-fns';
import MarketOrderData from './MarketOrderData';
import MarketOrderListTable from './MarketOrderListTable';
import MarketOrderHeader from './MarketOrderHeader';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Drawer from '../drawer/Drawer';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardTimePicker, KeyboardDatePicker } from '@material-ui/pickers';
import ArrowDownwardRoundedIcon from '@material-ui/icons/ArrowDownwardRounded';
import SearchRoundedIcon from '@material-ui/icons/SearchRounded';
import FindInPageRoundedIcon from '@material-ui/icons/FindInPageRounded';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: '#d0ccff',
        display: 'flex',
        padding: theme.spacing(1, 2),
        marginTop: '3px', 
        marginBottom: '12px',
        '& .MuiButtonGroup-root': {
            margin: theme.spacing(0, 1),
        },
    },
    paper: {
        padding: theme.spacing(2),
        margin: theme.spacing(0),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    tabs: {
        borderRight: `1px solid ${theme.palette.divider}`,
    },
    tabPanel: {
        textAlign: 'left',
        '& > div': {
            padding: '0 !important',
        },
    },
    tabPanelInfo: {
        '& .MuiBox-root': {
            padding: theme.spacing(2),
            margin: theme.spacing(1, 0),
        },
    },
    tabPanelCs: {
        '& .MuiBox-root': {
            padding: theme.spacing(0),
        },
    },
    divide: {
        margin: theme.spacing(2, 0),
    },
    searchType: {
        '& .MuiSelect-select': {
            padding: '10.5px',
        },
    },
    dtPicker: {
        '& .MuiOutlinedInput-adornedEnd': {
            padding: theme.spacing(0),
            fontSize: '13px',
        },
    },
    btnGroup: {
        display: 'flex',
        textAlign: 'left',
        '& > *': {
            margin: theme.spacing(0),
        },
    },
    dtBtn: {
        minWidth: '35px',
        marginTop: '0px',
        paddingTop: '0px',
        paddingBottom: '0px',
    },
    innerTextField: {
        '& .MuiInputBase-multiline': {
            fontSize: '14px',
            padding: '9px 8px',
        },
    },
}));

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        className={useStyles().tabPanelInfo}
        role="tabpanel"
        hidden={value !== index}
        id={`vertical-tabpanel-${index}`}
        aria-labelledby={`vertical-tab-${index}`}
        {...other}
        style={{textAlign: 'left'}}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

  function a11yProps(index) {
    return {
      id: `vertical-tab-${index}`,
      'aria-controls': `vertical-tabpanel-${index}`,
    };
  }

export default function MarketOrderView() {
    const classes = useStyles();
    const [orderer, setOrderer] = useState(0);
    const [log, setLog] = useState(0);
    const [orderInfo, setOrderInfo] = useState(0);
    const [orderList, setOrderList] = useState(MarketOrderData);
    const [orderState, setOrderState] = useState('ALL');
    const [comment, setComment] = useState('남겨진 코멘트가 없습니다.');
    const [orderObj, setOrderObj] = useState({});
    const [state, setState] = useState({
        age: '',
        name: 'hai',
    });
    const [selectedDate, setSelectedDate] = useState(new Date('2020-08-18T21:11:54'));
    const [nodeId, setNodeId] = useState(0);

    const handleChangeOrderer = (event, newValue) => {
        setOrderer(newValue);
    };

    const handleChangeLog = (event, newValue) => {
        setLog(newValue);
    };

    const handleChangeOrderInfo = (event, newValue, orderState) => {
        setOrderInfo(newValue);
    };

    const handleRowClick = (row) => {
        setOrderObj(row);
        setComment(row.comment);
    };

    const handleChangeSearchType = (event) => {
        const name = event.target.name;
        setState({
          ...state,
          [name]: event.target.value,
        });
      };

    const handleDateChange = (date) => {
        setSelectedDate(date);
    };

    function orderInfoGreeting(ordInf) {
        // if(ordInf.goodsNo === undefined) {
        //     return '주문건을 선택하여 주십시오.';
        // } else {
            return (
                <Grid container spacing={3}>
                    <Grid xs={4} spacing={1}>
                        주문상태 : {ordInf.state} &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        배송방법 : {ordInf.deliType} &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        배송비 : {ordInf.deliPrice} 원 &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        주문위치 : {ordInf.marketType} &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        채널명 : {ordInf.channelNm} &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        채널번호 : {ordInf.channelNo} &nbsp;
                    </Grid>
                    <Grid xs={12} spacing={1}>
                        <Divider className={classes.divide}/>
                    </Grid>
                    <Grid xs={8} spacing={1}>
                        상품명 : {ordInf.goodsNm} &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        옵션 : {ordInf.goodsNo}
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        상품코드 : {ordInf.goodsNo}
                    </Grid>
                    <Grid xs={3} spacing={1}>
                        주문번호: {ordInf.orderId}
                    </Grid>
                    <Grid xs={3} spacing={1}>
                        개별주문번호: {ordInf.eachOrderId}
                    </Grid>
                </Grid>
            )
        // }
    }

    function receiverInfoGreeting(ordInf) {
        // if(ordInf.goodsNo === undefined) {
        //     return '주문건을 선택하여 주십시오.';
        // } else {
            return (
                <Grid container spacing={3}>
                    <Grid xs={4} spacing={1}>
                        주문자 : {ordInf.orderer} &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        주문자 연락처1 : {ordInf.ordererPhone} &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        주문자 연락처2 : {ordInf.ordererCellPhone} &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        수령인 : {ordInf.receiver} &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        수령인 연락처1 : {ordInf.receiverPhone} &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        수령인 연락처2 : {ordInf.receiverCellPhone} &nbsp;
                    </Grid>
                    <Grid xs={8} spacing={1}>
                        수령지 주소 : ({ordInf.receiverZipcode}) {ordInf.receiverAddressOne} {ordInf.receiverAddressTwo} &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        배송 메시지 : {ordInf.deliMsg} &nbsp;
                    </Grid>
                    <Grid xs={12} spacing={1}>
                        <Divider className={classes.divide}/>
                    </Grid>
                    <Grid xs={6} spacing={1}>
                        택배사 : {ordInf.deliCompanyNm} &nbsp;
                    </Grid>
                    <Grid xs={4} spacing={1}>
                        송장번호 : {ordInf.invoice}
                    </Grid>
                </Grid>
            )
        // }
    }

    const marketSelector = (nodeId) => {
        setNodeId(nodeId);
    }

    const handleClickOrderState = (orderState) => {
        setOrderState(orderState);
    }

    return (
        <div className={classes.root}>
            <Grid container spacing={1}>
                <Grid item xs={12} spacing={5}>
                    <Paper className={classes.paper}>
                        <Grid container spacing={1}>
                            <Drawer marketSelector={marketSelector}/>
                            <Grid item xs={2} spacing={1}>
                                <TextField
                                    label="검색"
                                    id="outlined-size-small"
                                    variant="outlined"
                                    size="small"
                                />
                                <FormControl variant="outlined" className={classes.formControl}>
                                    <InputLabel htmlFor="outlined-age-native-simple">Type</InputLabel>
                                    <Select
                                    className={classes.searchType}
                                    native
                                    value={state.age}
                                    onChange={handleChangeSearchType}
                                    label="type"
                                    inputProps={{
                                        name: 'age',
                                        id: 'outlined-age-native-simple',
                                    }}
                                    defaultValue="전체"
                                    >
                                    <option value={'ALL'}>전체</option>
                                    <option value={'goodsNm'}>상품명</option>
                                    <option value={'goodsNo'}>상품코드</option>
                                    </Select>
                                </FormControl>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                        className={classes.dtPicker}
                                        margin="normal"
                                        id="date-picker-dialog"
                                        label="시작"
                                        inputVariant="outlined"
                                        format="yyyy-MM-dd"
                                        value={selectedDate}
                                        onChange={handleDateChange}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                        size='small'
                                        style={{ width: '148px', marginTop: '8px', marginBottom: '0px' }}
                                        />
                                        <KeyboardDatePicker
                                        className={classes.dtPicker}
                                        margin="normal"
                                        id="date-picker-dialog"
                                        label="끝"
                                        inputVariant="outlined"
                                        format="yyyy-MM-dd"
                                        value={selectedDate}
                                        onChange={handleDateChange}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                        size='small'
                                        style={{ width: '148px', marginTop: '8px', marginBottom: '0px' }}
                                    />
                                </MuiPickersUtilsProvider>
                                <Button size="small" className={classes.dtBtn}>
                                    오늘
                                </Button>
                                <Button size="small" className={classes.dtBtn}>
                                    어제
                                </Button>
                                <Button size="small" className={classes.dtBtn}>
                                    1주
                                </Button>
                                <Button size="small" className={classes.dtBtn}>
                                    2주
                                </Button>
                                <Button size="small" className={classes.dtBtn}>
                                    1달
                                </Button>
                                <Button size="small" className={classes.dtBtn}>
                                    3달
                                </Button>
                                <Button size="small" className={classes.dtBtn}>
                                    6달
                                </Button>
                                <Button size="small" className={classes.dtBtn}>
                                    1년
                                </Button>
                            </Grid>
                            <Grid item xs={1} spacing={1} style={{ textAlign: 'left' }}>
                                <Button variant="contained" color="primary" style={{ height: '100%' }}>
                                    <p>
                                        <SearchRoundedIcon /><br />
                                        <span>검색</span>
                                    </p>
                                </Button>
                                <Button variant="contained" color="primary" style={{ height: '100%', padding: '6px 6px', marginLeft: '12px' }}>
                                    <p>
                                        <FindInPageRoundedIcon /><br />
                                        <span>상세검색</span>
                                    </p>
                                </Button>
                            </Grid>
                            <Divider orientation="vertical" flexItem />
                            <Grid item xs={5} spacing={0} className={classes.btnGroup}>
                                <Button 
                                    onClick={() => handleClickOrderState('ALL')}
                                    variant="outlined"
                                    color="primary"
                                    style={{ height: '100%', padding: '6px 6px', marginLeft: '6px' }}
                                >
                                    전체보기<br />
                                    (8480)
                                </Button>
                                <ButtonGroup
                                    orientation="vertical"
                                    color="primary"
                                    aria-label="vertical outlined primary button group"
                                    variant="contained"
                                    style={{ marginLeft: '16px' }}
                                >
                                    <Button onClick={() => handleClickOrderState('NEW')} style={{backgroundColor: '#ADAADD'}}>신규주문(865)</Button>
                                    <Button onClick={() => handleClickOrderState('READY')} style={{backgroundColor: '#847fd8'}}>주문확인(12)</Button>
                                    <Button style={{backgroundColor: '#6c63d6'}}>보류(50)</Button>
                                </ButtonGroup>
                                <ButtonGroup
                                    orientation="vertical"
                                    color="primary"
                                    aria-label="vertical outlined primary button group"
                                    variant="contained"
                                >
                                    <Button style={{backgroundColor: '#d091ff', borderColor: '#b400ff'}}>송장출력(1859)</Button>
                                    <Button style={{backgroundColor: '#b450ff', borderColor: '#b400ff'}}>송장입력(0)</Button>
                                    <Button style={{backgroundColor: '#a22cfb', borderColor: '#b400ff'}}>출고(110)</Button>
                                </ButtonGroup>
                                <ButtonGroup
                                    orientation="vertical"
                                    color="primary"
                                    aria-label="vertical outlined primary button group"
                                    variant="contained"
                                >
                                    <Button onClick={() => handleClickOrderState('DELIVERING')} style={{backgroundColor: '#80c583', borderColor: '#009e05'}}>배송중(3089)</Button>
                                    <Button style={{backgroundColor: '#4abd4f', borderColor: '#009e05'}}>수취확인(290)</Button>
                                    <Button style={{backgroundColor: '#18b11f', borderColor: '#009e05'}}>정산완료(1200)</Button>
                                </ButtonGroup>
                                <ButtonGroup
                                    orientation="vertical"
                                    color="secondary"
                                    aria-label="vertical outlined primary button group"
                                    variant="contained"
                                >
                                    <Button style={{backgroundColor: '#ff7979'}}>취소(0)</Button>
                                    <Button style={{backgroundColor: '#ff4545'}}>반품요청(14)</Button>
                                    <Button style={{backgroundColor: '#ff1e1e'}}>교환요청(1)</Button>
                                </ButtonGroup>
                                <ButtonGroup
                                    orientation="vertical"
                                    color="secondary"
                                    aria-label="vertical outlined primary button group"
                                    variant="contained"
                                >
                                    <Button style={{backgroundColor: '#dea366'}}>취소마감(124)</Button>
                                    <Button style={{backgroundColor: '#ec7c3a'}}>반품마감(421)</Button>
                                    <Button style={{backgroundColor: '#fb6005'}}>교환마감(6)</Button>
                                </ButtonGroup>
                            </Grid>
                            <Divider orientation="vertical" flexItem />
                            <Grid item xs={2} spacing={0} style={{ textAlign: 'left' }}>
                                <Button variant="contained" color="primary" style={{ height: '100%', marginLeft: '6px' }}>
                                <p>
                                    <ArrowDownwardRoundedIcon /><br />
                                    <span>가져오기</span>
                                </p>
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                <Grid item xs={8} spacing={5}>
                    <Paper className={classes.paper}>
                        <AppBar position="static">
                            <Tabs value={orderer} onChange={handleChangeOrderer} aria-label="simple tabs example">
                                <Tab label="상품정보" {...a11yProps(0)} />
                                <Tab label="발주정보" {...a11yProps(1)} />
                            </Tabs>
                        </AppBar>
                        <TabPanel value={orderer} index={0} className={classes.tabPanelInfo}>
                            {orderInfoGreeting(orderObj)}
                        </TabPanel>
                        <TabPanel value={orderer} index={1}>
                            {receiverInfoGreeting(orderObj)}
                        </TabPanel>
                    </Paper>
                </Grid>
                <Grid item xs={4} spacing={1}>
                    <Paper className={classes.paper}>
                        <TabPanel className={classes.tabPanelCs} value={log} index={0}>
                            <Paper elevation={0} style={{ height: '99px', fontSize: '14px' }}>
                                {
                                    comment.split('\n').map( line => {
                                        return (<span>{line}<br/></span>)
                                    })
                                }
                            </Paper>
                            <TextField
                                className={classes.innerTextField}
                                id="outlined-multiline-static"
                                label="COMMENT"
                                fullWidth={true}
                                multiline
                                rows={3}
                                placeholder="CS 코멘트"
                                variant="outlined"
                            />
                            <Button variant="contained" fullWidth={true} color="primary">
                                저장
                            </Button>
                        </TabPanel>
                        <TabPanel value={log} index={1}>
                            로그......
                        </TabPanel>
                    </Paper>
                </Grid>
                <Grid item xs={12} spacing={1}>
                    <Paper className={classes.paper}>
                        <MarketOrderListTable orderState={orderState} orderMarket={nodeId} handleRowClick={handleRowClick} style={{ padding: '0px' }}/>
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}