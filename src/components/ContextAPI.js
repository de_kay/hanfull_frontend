import { createContext } from "react";

// ContextAPI 생성
const TokenContext = createContext(null);

// ContextAPI EXPORT
export default TokenContext;