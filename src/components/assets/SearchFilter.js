const searchFilter = (event, marketData) => {
    let term = event.target.value.replace(/[&\\#,+()$~%.'":*?<>{}]/g,'_').toLowerCase();
    if(term.length > 0) {
        let filterList = marketData.filter(function(item) {
            return item.label.replace(/[&\\#,+()$~%.'":*?<>{}]/g,'_').toLowerCase().search(term) !== -1;
        });
        return filterList;
    } else {
        return marketData;
    }
}

export default searchFilter;