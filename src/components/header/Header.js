import React from 'react';
import NavBar from '../navbar/NavBar';
import { withRouter, Redirect } from 'react-router-dom'

class Header extends React.Component {
    render() {
        if (this.props.token === 'expiredToken') {
            return <Redirect to='/logout' />
        } else {
            if (this.props.history.location.pathname === '/login' || this.props.history.location.pathname === '/sign-up') {
                return null;
            } else {
                return (
                    <div className="Header" style={{ minHeight: '50px'}}>
                        <div>
                            <NavBar />
                        </div>
                    </div>
                );
            }
        }
    }
}
export default withRouter(Header);