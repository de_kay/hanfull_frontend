import React, { useState, useContext } from 'react';
import clsx from 'clsx';
import marketData from './data';
import searchFilter from '../assets/SearchFilter';
import { makeStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import TreeItem from '@material-ui/lab/TreeItem';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from "@material-ui/core/InputAdornment";
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  root: {
    height: 240,
    flexGrow: 1,
    maxWidth: 400,
  },
  columnCenter: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  drawerTop: {
    textAlign: 'center',
    margin: 'auto',
    padding: 20
  },
  divideSpacing: {
    marginTop: 10,
    marginBottom: 10
  }
});

const barLocation = 'left';

export default function SwipeableTemporaryDrawer(props) {
  const classes = useStyles();
  const [search, setSearch] = useState(marketData);
  const [state, setState] = useState({
    right: false
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event && event.type === 'keydown') {
      return;
    }
    setSearch(marketData);
    setState({ state, [anchor]: open });
  };

  function doSearch(event) {
    setSearch(searchFilter(event, marketData));
  }

  function viewDrawerMenuList(search) {
      return (
          search.map((data) => (
            <form>
              {data.devide ? <Divider className={classes.divideSpacing} /> : ''}
              <TreeItem nodeId={data.nodeId.toString()} onClick={() => props.marketSelector(data.nodeId)} label={data.label} >
                  {data.child.map((childOne) => (
                          <TreeItem nodeId={childOne.nodeId.toString()} label={childOne.label} >
                              {childOne.child.map((childTwo) => (
                                    <TreeItem nodeId={childTwo.nodeId.toString()} onClick={() => props.marketSelector(childTwo.nodeId)} label={childTwo.label} />
                                  )
                                  )
                              }
                          </TreeItem>
                      )
                      )
                  }
              </TreeItem>
              </form>
          )
          )
      )
  }

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom',
      })}
      role="presentation"
    //   onClick={toggleDrawer(anchor, true)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
        <div className={classes.drawerTop}>
            <Avatar style={{margin: 'auto'}}>H</Avatar>
            <span>H님 로그인 하셨습니다.</span>
        </div>
        <TreeView
        className={classes.root}
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpandIcon={<ChevronRightIcon />}
        >
        <Divider className={classes.divideSpacing} />
        <Grid
            style={{marginBottom: 10}}
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
        >
        <TextField
            id="outlined-size-small"
            placeholder="Search..."
            onChange={doSearch}
            InputProps={{
                startAdornment: (
                <InputAdornment>
                    <SearchIcon />
                </InputAdornment>
                )
            }}
        />
        </Grid>
        <TreeItem nodeId={0} onClick={() => props.marketSelector(0)} label='전체보기' />
        {viewDrawerMenuList(search)}
        </TreeView>
    </div>
  );

  return (
    <div className={classes.columnCenter}>
        <React.Fragment key={barLocation}>
            <IconButton
            aria-label="more"
            aria-controls="long-menu"
            aria-haspopup="true"
            onClick={toggleDrawer({barLocation}, true)}
            >
              <MoreVertIcon style={{ color: '#444444' }}/>
            </IconButton>
            <SwipeableDrawer
            disableSwipeToOpen={true}
            anchor={barLocation}
            open={state[{barLocation}]}
            onClose={toggleDrawer({barLocation}, false)}
            onOpen={toggleDrawer({barLocation}, true)}
            >
            {list({barLocation})}
          </SwipeableDrawer>
        </React.Fragment>
    </div>
  );
}