const marketData = [
    {
        
        devide: true,
        nodeId: 1,
        label: '오픈마켓',
        child: [{
            nodeId: 101,
            label: '옥션',
            child: [
                {
                    nodeId: 10101,
                    label: '한가득영농회'
                },
            ]
        },
        {
            nodeId: 102,
            label: 'G마켓',
            child: [
                {
                    nodeId: 10201,
                    label: '한가득영농회'
                },
            ]
        },
        {
            nodeId: 103,
            label: 'SSG',
            child: [
                {
                    nodeId: 10301,
                    label: '한가득영농회'
                },
            ]
        },
        {
            nodeId: 104,
            label: '스마트스토어',
            child: [
                {
                    nodeId: 10401,
                    label: '사랑영농회'
                },
                {
                    nodeId: 10402,
                    label: '열정농부'
                },
            ]
        },
        {
            nodeId: 105,
            label: '11번가',
            child: [
                {
                    nodeId: 10501,
                    label: '한가득영농회'
                },
            ]
        },
        {
            nodeId: 106,
            label: '쿠팡',
            child: [
                {
                    nodeId: 10601,
                    label: '한가득영농회'
                },
            ]
        },
        {
            nodeId: 107,
            label: '카카오스토어',
            child: [
                {
                    nodeId: 10701,
                    label: '한가득영농회'
                },
            ]
        },
        {
            nodeId: 108,
            label: '위메프',
            child: [
                {
                    nodeId: 10801,
                    label: '한가득영농회'
                },
            ]
        }]
    },
    {
        devide: false,
        nodeId: 2,
        label: '독립몰',
        child: [{
            nodeId: 201,
            label: '고도몰5',
            child: [
                {
                    nodeId: 20101,
                    label: '열정농부'
                },
            ]
        },
        {
            nodeId: 202,
            label: 'KimsOn',
            child: [
                {
                    nodeId: 20201,
                    label: '하동사랑농장'
                },
                {
                    nodeId: 20202,
                    label: '남도어부촌'
                },
                {
                    nodeId: 20203,
                    label: '홍천마을'
                },
                {
                    nodeId: 20204,
                    label: '행복한해남농장'
                },
                {
                    nodeId: 20205,
                    label: '순창김옥희할머니'
                },
                {
                    nodeId: 20206,
                    label: '큰집농장'
                },
                {
                    nodeId: 20207,
                    label: '고창행봉녹장'
                },
                {
                    nodeId: 20208,
                    label: '문경촌농장'
                },
                {
                    nodeId: 20209,
                    label: '제주맘농장'
                },
                {
                    nodeId: 20210,
                    label: '영희네농장'
                },
                {
                    nodeId: 20211,
                    label: '나주따봉농장'
                },
                {
                    nodeId: 20212,
                    label: '성주참외농장'
                },
                {
                    nodeId: 20213,
                    label: '참숯공원'
                },
                {
                    nodeId: 20214,
                    label: '서호농장'
                },
                {
                    nodeId: 20215,
                    label: '포항행복수산'
                },
                {
                    nodeId: 20216,
                    label: '고흥행복농장'
                },
            ]
        },
        {
            nodeId: 203,
            label: '영카트5',
            child: [
                {
                nodeId: 20301,
                label: '열정농부'
            }
        ]
        },
        {
            nodeId: 204,
            label: '스룩페이',
            child: [
                {
                    nodeId: 20401,
                    label: '한가득영농회'
                }
            ]
        }]
    },
    {
        devide: true,
        nodeId: 3,
        label: '즐겨찾기',
        child: []
    }
];

export default marketData;