///// For IE11 Import @dykwon ///// 
// import 'core-js';
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
// import "@babel/polyfill";
//////////////////////////////////

////// Korean Fonts @dykwon //////
import './assets/fonts/GodoB.woff';
import './assets/fonts/GodoB.woff2';
import './assets/fonts/GodoM.woff';
import './assets/fonts/GodoM.woff2';
//////////////////////////////////

import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/index.css';
import App from './views/App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
<App />, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
