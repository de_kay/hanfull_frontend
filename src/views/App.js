import React from 'react';
import '../assets/css/App.css';
import dotenv from 'dotenv';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Login from './Login';
import Logout from './Logout';
import Header from "../components/header/Header";
import Footer from "../components/footer/Footer"
import Box from '@material-ui/core/Box'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import cookie from 'react-cookies';
import { createBrowserHistory as createHistory } from 'history';

//PAGE IMPORT
import Home from "./Home";
import Order from "./Order";

dotenv.config();

const theme = createMuiTheme({
  typography: {
    fontFamily: 'Godo',
  },
});

function isExpiredToken(jwtToken){
  const base64 = require('base-64');
  const utf8 = require('utf8');
  const jwtTokenArr = jwtToken.split('.');
  const jwtTokenBodyDecoded = JSON.parse(utf8.decode(base64.decode(jwtTokenArr[1])));
  
  const curDateTime = new Date();
  const expireDateTime = new Date(jwtTokenBodyDecoded.exp*1000);

  if(expireDateTime < curDateTime){
    return true;
  }else{
    return false;
  }
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token : cookie.load('token') !== undefined ? cookie.load('token') : undefined,
      userKey : undefined,
      nickName : undefined,
      userID : undefined,
      nodeID : 0,
      selectNode: this.selectNode,
    };

    this.selectNode = () => {
      this.setState(state => ({
        nodeID: state.nodeID,
      }));
      console.log(this.nodeID);
    }

  }

  setObjectFromToken = (jwtToken) => {
    const base64 = require('base-64');
    const utf8 = require('utf8');
    const jwtTokenArr = jwtToken.split('.');
    const jwtTokenBodyDecoded = JSON.parse(utf8.decode(base64.decode(jwtTokenArr[1])));
    this.setState({userKey:jwtTokenBodyDecoded.jti});
    this.setState({nickName:jwtTokenBodyDecoded.nickName})
    this.setState({userID:jwtTokenBodyDecoded.sub})
  }

  componentDidMount(){
    // 쿠키값이 있는지 확인 후, 있으면 토큰을 state에 세팅
    if(cookie.load('token') !== undefined){
      //만일 토큰이 Expired 되었다면 logout 페이지로 이동
      if(isExpiredToken(cookie.load('token'))){
        this.setState({token : 'expiredToken'});
      }else{
        this.setState({token : cookie.load('token')});
        this.setObjectFromToken(cookie.load('token'))
      }
    // 없으면 undefined를 state에 세팅
    }else{
      this.setState({token : undefined})
    }
  }

  render() {
    const history = createHistory();   
    const { token } = this.state;
    const { userKey } = this.state;
    const { nickName } = this.state;
    const { userID } = this.state;
    const { nodeID } = this.state;
    const PrivateRoute = ({ component: Component, ...rest }) => (
      <Route {...rest} render={(props) => (
        token !== undefined ? <Component {...props} /> : <Redirect to='/login' />
      )} />
    )
    
    return (
      <div className="App">
        <div>
          <Box>
            {/* <TokenContext.Provider value={{token, userKey, nickName, userID}}> */}
              <MuiThemeProvider theme={theme}>
                <Router history={history}>
                  <Header token={token} />
                  <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/logout" component={Logout} />
                    <Route exact path="/order" component={Order} />
                  </Switch>
                  <Footer />
                </Router>
              </MuiThemeProvider>
            {/* </TokenContext.Provider> */}
          </Box>
        </div>
      </div>
    );
  }
}

export default App;
