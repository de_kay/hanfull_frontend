import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container'
import { Helmet } from 'react-helmet'

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(0, 2),
  }
}));

export default function Home() {
  const classes = useStyles();
  
  const isIE = /*@cc_on!@*/false || !!document.documentMode;
  const isEdge = !isIE && !!window.StyleMedia;

  return (
        <React.Fragment>
          <Helmet>
          <title>HAN | 한가득영농회 판매관리 플랫폼</title>
          <meta name="description" content="한가득영농회 판매관리 플랫폼" />
          <meta name="keywords" content="HAN" />
          <meta name="author" content="HAN" />
          </Helmet>
          <CssBaseline />
          <Paper className={classes.root}>
            <Container maxWidth="false">
            </Container>
          </Paper>
        </React.Fragment>
  );
}