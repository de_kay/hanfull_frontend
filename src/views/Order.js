import React from 'react';
import { Helmet } from 'react-helmet'
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import MarketOrderView from '../components/body/MarketOrderView';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(0, 2),
    },
}));

export default function Order() {
    return (
        <React.Fragment>
            <Helmet>
                <title>HAN | 한가득영농회 판매관리 플랫폼</title>
                <meta name="description" content="한가득영농회 판매관리 플랫폼" />
                <meta name="keywords" content="HAN" />
                <meta name="author" content="HAN" />
            </Helmet>
            <CssBaseline />
            <MarketOrderView />
        </React.Fragment>
    )
}